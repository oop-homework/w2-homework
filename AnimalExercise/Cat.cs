﻿using System;

namespace AnimalExercise
{
    public class Cat : Animal
    {
        public Cat(int age, string name, char gender)
            : base(age, name, gender)
        {

        }

        public override void MakeSound()
        {
            Console.WriteLine($"{this.Name} goes meow meow!");
        }
    }
}
