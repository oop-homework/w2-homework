﻿namespace AnimalExercise
{
    public abstract class Animal
    {
        public int Age { get; set; }

        public string Name { get; set; }

        public char Gender { get; set; }

        protected Animal(int age, string name, char gender)
        {
            this.Age = age;
            this.Name = name;
            this.Gender = gender;
        }

        protected Animal(int age, string name)
        {
            this.Age = age;
            this.Name = name;
        }

        public abstract void MakeSound();

        public override string ToString()
        {
            return $"{this.Name} is a {(this.Gender == 'M' ? "male" : "female")} that is {this.Age} years old.";
        }
    }
}
