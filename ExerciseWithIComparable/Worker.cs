﻿using System;

namespace ExerciseWithIComparable
{
    class Worker : Human, IComparable
    {
        public double Wage { get; set; }

        public int WorkedHours { get; set; }

        public Worker(string firstName, string lastName, double wage, int workedHours)
            : base(firstName, lastName)
        {
            this.Wage = wage;
            this.WorkedHours = workedHours;
        }

        //Не съм сигурен дали така се смята надница/час, но да
        public double HourlyWage()
        {
            return Math.Round(this.Wage / this.WorkedHours, 2);
        }

        //взето от документацията на Microsoft за IComparable
        public int CompareTo(object obj)
        {
            if (obj == null)
            {
                return 1;
            }

            Worker worker = obj as Worker; //това не съм сигурен какво точно прави (имам предположение, че казва на obj да стане Worker
            if (worker != null)
            {
                return this.Wage.CompareTo(worker.Wage);
            }

            throw new ArgumentException("Object is not a Worker!");
        }
    }
}
