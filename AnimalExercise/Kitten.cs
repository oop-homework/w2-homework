﻿using System;

namespace AnimalExercise
{
    public class Kitten : Animal
    {
        public Kitten(int age, string name, char gender)
            : base(age, name, gender)
        {

        }

        public override void MakeSound()
        {
            Console.WriteLine($"{this.Name} goes little meow meow!");
        }
    }
}
