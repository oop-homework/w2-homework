﻿using System;

namespace AnimalExercise
{
    public class Dog : Animal
    {
        public Dog(int age, string name, char gender) 
            : base(age, name, gender)
        {

        }

        public override void MakeSound()
        {
            Console.WriteLine($"{this.Name} goes bark bark!");
        }
    }
}
