﻿using System;
using System.Collections.Generic;

namespace ExerciseWithIComparable
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Worker> workers = InputWorkers();
            List<Student> students = InputStudents();

            students.Sort();
            students.ForEach(s => Console.WriteLine($"{s.FirstName} {s.LastName} has a grade of {s.Grade}"));

            //понеже се иска работниците да се сортират по заплата в намаляващ ред,
            //аз ги сортирам в нарастващ и след това обръщам листа
            workers.Sort();
            workers.Reverse();
            workers.ForEach(w => Console.WriteLine($"{w.FirstName} {w.LastName} has ${w.Wage} wage. They have worked for {w.WorkedHours}. " +
                                                   $"Therefore they earn ${w.HourlyWage()}/hr."));
        }

        private static List<Student> InputStudents()
        {
            List<Student> students = new List<Student>();
            Console.Write("Number of students: ");
            int n = int.Parse(Console.ReadLine());
            for (int i = 1; i <= n; i++)
            {
                Console.Write($"Student {i}'s first name: ");
                string firstName = Console.ReadLine();

                Console.Write($"Student {i}'s last name: ");
                string lastName = Console.ReadLine();

                Console.Write($"Student {i}'s grade: ");
                double grade = double.Parse(Console.ReadLine());

                students.Add(new Student(firstName, lastName, grade));
            }

            return students;
        }

        private static List<Worker> InputWorkers()
        {
            List<Worker> workers = new List<Worker>();
            Console.Write("Number of workers: ");
            int n = int.Parse(Console.ReadLine());
            for (int i = 1; i <= n; i++)
            {
                Console.Write($"Worker {i}'s first name: ");
                string firstName = Console.ReadLine();

                Console.Write($"Worker {i}'s last name: ");
                string lastName = Console.ReadLine();

                Console.Write($"Worker {i}'s wage: ");
                double wage = double.Parse(Console.ReadLine());

                Console.Write($"Worker {i}'s worked hours: ");
                int workedHours = int.Parse(Console.ReadLine());

                workers.Add(new Worker(firstName, lastName, wage, workedHours));
            }

            return workers;
        }
    }
}
