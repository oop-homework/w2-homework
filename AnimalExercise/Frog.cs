﻿using System;

namespace AnimalExercise
{
    public class Frog : Animal
    {
        public Frog(int age, string name, char gender)
            : base(age, name, gender)
        {

        }

        public override void MakeSound()
        {
            Console.WriteLine($"{this.Name} goes croak croak!");
        }
    }
}
