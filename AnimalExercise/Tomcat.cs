﻿using System;

namespace AnimalExercise
{
    public class Tomcat : Animal
    {
        public Tomcat(int age, string name)
            : base(age, name)
        {
            this.Gender = 'M';
        }

        public override void MakeSound()
        {
            Console.WriteLine($"{this.Name} goes tomcat meow meow!");
        }
    }
}
