﻿using System;

namespace ExerciseWithIComparable
{
    class Student : Human, IComparable
    {
        public double Grade { get; set; }

        public Student(string firstName, string lastName, double grade) 
            : base(firstName, lastName)
        {
            this.Grade = grade;
        }

        //взето от документацията на Microsoft за IComparable
        public int CompareTo(object obj)
        {
            if (obj == null)
            {
                return 1;
            }

            Student student = obj as Student;
            if (student != null)
            {
                return this.Grade.CompareTo(student.Grade);
            }

            throw new ArgumentException("Object is not a Worker!");
        }
    }
}
