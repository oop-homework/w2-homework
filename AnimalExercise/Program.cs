﻿using System;
using System.Collections.Generic;

namespace AnimalExercise
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Animal> animals = new List<Animal>();

            Dog dog = new Dog(2, "Шаро", 'M');
            animals.Add(dog);

            Frog frog = new Frog(4, "Милен", 'M');
            animals.Add(frog);

            Cat cat = new Cat(5, "Селина", 'F');
            animals.Add(cat);

            Kitten kitten = new Kitten(1, "Кати", 'F');
            animals.Add(kitten);

            Tomcat tomcat = new Tomcat(3, "Тошко");
            animals.Add(tomcat);

            animals.ForEach(a => Console.WriteLine(a));
            Console.WriteLine("------------------------------");
            animals.ForEach(a => a.MakeSound());
        }
    }
}
